(async () => {
	if (window.location.pathname != '/') return;
	let storage = await getDataSourceForDomain(window.location);
	let opts = await storage.get(['tetrioPlusEnabled', 'enableVerge']);
	if (!opts.tetrioPlusEnabled || !opts.enableVerge) return;

	let script = document.createElement('script');
	script.src = browser.runtime.getURL("source/injected/verge.js");
	document.head.appendChild(script);
})().catch(console.error)