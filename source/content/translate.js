(async () => {
    if (window.location.pathname != '/') return;
    let storage = await getDataSourceForDomain(window.location);
    let opts = await storage.get(['tetrioPlusEnabled', 'enableTranslate']);
    if (!opts.tetrioPlusEnabled || !opts.enableTranslate) return;

    const script = document.createElement('script')
    script.setAttribute('src', 'https://greasyfork.org/scripts/466016-tetr-io中文翻译/code/TETRIO中文翻译.user.js')
    document.head.appendChild(script)
})().catch(console.error)